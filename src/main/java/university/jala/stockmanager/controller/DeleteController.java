package university.jala.stockmanager.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import university.jala.stockmanager.service.DeleteService;

@RestController
@RequestMapping("/delete")
public class DeleteController {

    @GetMapping
    public ResponseEntity deleteProduct(@RequestParam int id) {
        return ResponseEntity.ok(DeleteService.removeProduct(id));
    }
}
