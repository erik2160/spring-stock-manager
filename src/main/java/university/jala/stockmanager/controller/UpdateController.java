package university.jala.stockmanager.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import university.jala.stockmanager.service.UpdateService;

@RestController
@RequestMapping("/update")
public class UpdateController {

    @GetMapping("/name")
    public ResponseEntity changeName(@RequestParam int id, @RequestParam String name) {
        return ResponseEntity.ok(UpdateService.updateName(id, name));
    }

    @GetMapping("/description")
    public ResponseEntity changeDescription(@RequestParam int id, @RequestParam String description) {
        return ResponseEntity.ok(UpdateService.updateDescription(id, description));
    }

    @GetMapping("/price")
    public ResponseEntity changePrice(@RequestParam int id, @RequestParam String price) {
        return ResponseEntity.ok(UpdateService.updatePrice(id, price));
    }

    @GetMapping("/quantity")
    public ResponseEntity changeQuantity(@RequestParam int id, @RequestParam String quantity) {
        return ResponseEntity.ok(UpdateService.updateQuantity(id, quantity));
    }
}
