package university.jala.stockmanager.controller;


import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import university.jala.stockmanager.service.MenuService;

@RestController
public class MenuController {

    @GetMapping("/")
    public String startMenuMessage() {
        return MenuService.showMenuMessage();
    }
}
