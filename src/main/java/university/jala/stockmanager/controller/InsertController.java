package university.jala.stockmanager.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import university.jala.stockmanager.service.InsertService;

@RestController
@RequestMapping("/insert")
public class InsertController {
    @GetMapping
    public ResponseEntity insertProduct(@RequestParam String name, @RequestParam String description, @RequestParam String price, @RequestParam String quantity) {
        try {
            InsertService.insertProduct(name, description, price, quantity);
            return ResponseEntity.ok("Produto de ID " + InsertService.getId() + " inserido com sucesso!");
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.ok("Erro ao adicionar produto.");
        }
        //http://localhost:8080/insert/name=Computer&description=NiceComputer&price=300&quantity=10
    }
}
