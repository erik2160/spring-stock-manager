package university.jala.stockmanager.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import university.jala.stockmanager.service.ListService;

@RestController
public class ListProductsController {

    @GetMapping("/list")
    public ResponseEntity listProducts() {
        return ResponseEntity.ok(ListService.returnProductsList());
    }
}
