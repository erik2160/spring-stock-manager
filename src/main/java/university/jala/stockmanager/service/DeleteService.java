package university.jala.stockmanager.service;

import university.jala.stockmanager.repository.ProductRepository;

public class DeleteService {
    public static String removeProduct(int id) {
        if (!ProductRepository.productHashMap.containsKey(id)) {
            return "ID Inválido";
        } else {
            ProductRepository.productHashMap.remove(id);
        }
        return "Produto de ID " + id + " removido com sucesso!";
    }
}
