package university.jala.stockmanager.service;

import university.jala.stockmanager.models.Product;
import university.jala.stockmanager.repository.ProductRepository;

public class InsertService {
    private static int id = 0;

    public static void insertProduct(String name, String description, String price, String quantity) {
        ProductRepository.productHashMap.put(id, new Product(name, description, price, quantity));
        id++;
    }

    public static int getId() {
        return id;
    }

    public static void setId(int id) {
        InsertService.id = id;
    }
}
