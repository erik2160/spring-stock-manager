package university.jala.stockmanager.service;

import university.jala.stockmanager.repository.ProductRepository;

public class ListService {

    public static String returnProductsList() {
        return ProductRepository.productHashMap.toString();
    }
}
