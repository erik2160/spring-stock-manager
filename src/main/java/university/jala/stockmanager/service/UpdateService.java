package university.jala.stockmanager.service;

import university.jala.stockmanager.repository.ProductRepository;

public class UpdateService {
    public static String updateName(int id, String name) {
        if (validarId(id)) {
            return "ID Inválido!";
        } else {
            ProductRepository.productHashMap.get(id).setName(name);
        }
        return "Nome do produto atualizado com sucesso!";
    }

    public static String updateDescription(int id, String description) {
        if (validarId(id)) {
            return "ID Inválido!";
        } else {
            ProductRepository.productHashMap.get(id).setDescription(description);
        }
        return "Descrição do produto atualizada com sucesso!";
    }

    public static String updatePrice(int id, String price) {
        if (validarId(id)) {
            return "ID Inválido!";
        } else {
            ProductRepository.productHashMap.get(id).setPrice(price);
        }
        return "Preço do produto atualizado com sucesso!";
    }

    public static String updateQuantity(int id, String quantity) {
        if (validarId(id)) {
            return "ID Inválido!";
        } else {
            ProductRepository.productHashMap.get(id).setQuantity(quantity);
        }
        return "Quantidade do produto atualizada com sucesso!";
    }

    public static boolean validarId(int id) {
        return id >= ProductRepository.productHashMap.size();
    }
}
